<?php

namespace Drupal\twig_n2w\Twig;

/**
 * Implements class Numbers2WordsConversion.
 */
class Numbers2WordsConversion extends \Twig_Extension {

  /**
   * Gets filters.
   *
   * @return array
   *   Returns array.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('n2w_format_bharat', [$this, 'n2wFormatBharat']),
      new \Twig_SimpleFilter('n2w_format_intl', [$this, 'n2wFormatIntl']),
    ];
  }

  /**
   * Implements getName().
   */
  public function getName() {
    return 'twig_n2w';
  }

  /**
   * Convert number into Bharat(Indian) words.
   *
   * @param mixed $input
   *   The Input.
   * @param string $format
   *   The Format.
   *
   * @return mixed
   *   Return statement.
   *
   *   For currency format
   *
   * @see https://twig.symfony.com/doc/2.x/filters/format_currency.html
   */
  public function n2wFormatBharat($input, $format = NULL) {

    if (empty($input)) {
      return '';
    }
    $input = (string) $input;
    // Explode the number from decimal point.
    $input_array = explode('.', $input);
    $number = $input_array[0];
    $decimal_number = !empty($input_array[1]) ? $input_array[1] : '';
    $number_array = [];
    // Calculate the number counters.
    while ($number != '') {
      // If number is greater than three digit value.
      if (strlen($number) > 3) {
        // Check whether number is two digit or one digit counter.
        $counter_value = ((strlen($number) % 2) == 0) ? 1 : 2;
        // Save above counter value in array.
        $number_array[] = substr($number, 0, $counter_value);
        // Reset number excluding the counter digit.
        $number = substr($number, $counter_value);
      }
      elseif (strlen($number) == 3) {
        // If number is three digit value than it will be hundred's value.
        $number_array[] = substr($number, 0, 1);
        $number = substr($number, 1);
      }
      else {
        // If number is two digit value than it will be ten's value.
        $number_array[] = substr($number, 0, 2);
        $number = substr($number, 2);
      }
    }

    // Set number in words for non decimal values.
    $value = '';
    $length = count($number_array);
    foreach ($number_array as $num) {
      if (!empty($num)) {
        $value .= $this->get_number($num);
        $value .= $this->get_counter_india($length);
      }
      $length--;
    }
    // Check wif currency passed as a parameter.
    if (!empty($format) && $format == 'currency') {
      $value .= 'Rupees ';
    }

    // Set number in words for decimal values.
    if (!empty($decimal_number)) {
      $decimal_number = round(".$decimal_number", 2);
      // Convert it.
      $decimal_word = $this->get_number($decimal_number * 100);
      if (!empty($format) && $format == 'currency') {
        $value .= 'and ' . $decimal_word . 'Paisa';
      }
      else {
        $value .= 'Point ' . $decimal_word;
      }
    }

    return $value;
  }

  /**
   * Convert number into international words.
   *
   * @param mixed $input
   *   The Input.
   * @param string $format
   *   The Format.
   *
   * @return mixed
   *   Return Statement.
   *
   *   For currency format
   *
   * @see https://twig.symfony.com/doc/2.x/filters/format_currency.html
   */
  public function n2wFormatIntl($input, $format = NULL) {

    if (empty($input)) {
      return '';
    }
    $input = (string) $input;
    // Explode the number from decimal point.
    $input_array = explode('.', $input);
    $number = $input_array[0];
    $decimal_number = !empty($input_array[1]) ? $input_array[1] : '';
    $number_array = [];
    // Calculate the number counters.
    while ($number != '') {
      // If number is greater than three digit value.
      if (strlen($number) >= 3) {
        // Check whether number is two digit or one digit counter.
        $counter_value = ((strlen($number) % 3) == 0) ? 1 : 2;
        // Save above counter value in array.
        $number_array[] = substr($number, 0, $counter_value);
        // Reset number excluding the counter digit.
        $number = substr($number, $counter_value);
      }
      else {
        // If number is two digit value than it will be ten's value.
        $number_array[] = substr($number, 0, 2);
        $number = substr($number, 2);
      }
    }

    // Set number in words for non decimal values.
    $value = '';
    $length = count($number_array);
    foreach ($number_array as $num) {
      if (!empty($num)) {
        $value .= $this->get_number($num);
        $value .= $this->get_counter_intl($length);
      }
      $length--;
    }

    // Check wif currency passed as a parameter.
    if (!empty($format) && $format == 'currency') {
      $value .= 'Dollars ';
    }

    // Set number in words for decimal values.
    if (!empty($decimal_number)) {
      $decimal_number = round(".$decimal_number", 2);
      // Convert it.
      $decimal_word = $this->get_number($decimal_number * 100);
      if (!empty($format) && $format == 'currency') {
        $value .= 'and ' . $decimal_word . 'Cents';
      }
      else {
        $value .= 'Point ' . $decimal_word;
      }
    }

    return $value;
  }

  /**
   * Implements getNumber().
   */
  public function getNumber($input) {
    switch ($input) {
      case 0:
        return '';

      case 1:
        return 'One ';

      case 2:
        return 'Two ';

      case 3:
        return 'Three ';

      case 4:
        return 'Four ';

      case 5:
        return 'Five ';

      case 6:
        return 'Six ';

      case 7:
        return 'Seven ';

      case 8:
        return 'Eight ';

      case 9:
        return 'Nine ';

      case 10:
        return 'Ten ';

      case 11:
        return 'Eleven ';

      case 12:
        return 'Twelve ';

      case 13:
        return 'Thirteen ';

      case 14:
        return 'Fourteen ';

      case 15:
        return 'Fifteen ';

      case 16:
        return 'Sixteen ';

      case 17:
        return 'Seventeen ';

      case 18:
        return 'Eighteen ';

      case 19:
        return 'Ninteen ';

      case 20:
        return 'Twenty-';

      case 30:
        return 'Thirty-';

      case 40:
        return 'Fourty-';

      case 50:
        return 'Fifty-';

      case 60:
        return 'Sixty-';

      case 70:
        return 'Seventy-';

      case 80:
        return 'Eighty-';

      case 90:
        return 'Ninty-';

      case 100:
        return 'Hundred ';

      default:
        // As we have max two digits number.
        $digit1 = (int) ($input / 10) * 10;
        $digit2 = $input % 10;
        // Call function itself to get values.
        return $this->get_number($digit1) . $this->get_number($digit2);
    }
  }

  /**
   * Get counter value as per the digit position.
   *
   * @param mixed $length
   *   Length.
   *
   * @return string
   *   Returns string.
   */
  public function getCounterIndia($length) {
    switch ($length) {
      case '10':
        return 'Shankh ';

      case '9':
        return 'Padm ';

      case '8':
        return 'Neel ';

      case '7':
        return 'Kharab ';

      case '6':
        return 'Arab ';

      case '5':
        return 'Crore ';

      case '4':
        return 'Lac ';

      case '3':
        return 'Thousand ';

      case '2':
        return 'Hundred ';
    }
  }

  /**
   * Get counter value as per the digit position.
   *
   * @param mixed $length
   *   Length.
   *
   * @return string
   *   Returns String.
   */
  public function getCounterIntl($length) {
    // 2nd position of digit will be hundred's
    if ($length % 2 == 0) {
      return 'Hundred ';
    }
    switch ($length) {
      case '21':
        return 'Nonillion ';

      case '19':
        return 'Octillion ';

      case '17':
        return 'Septillion ';

      case '15':
        return 'Sextillion ';

      case '13':
        return 'Quintillion ';

      case '11':
        return 'Quadrillion ';

      case '9':
        return 'Trillion ';

      case '7':
        return 'Billion ';

      case '5':
        return 'Million ';

      case '3':
        return 'Thousand ';
    }
  }

}
